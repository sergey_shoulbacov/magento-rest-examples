<?php
require_once 'include/init.php';
require_once 'include/config.php';

session_start();
session_destroy();

printf('<a href="%s">back to REST Customer example</a>', BASE_URL . 'rest.php');
print('<br>');
printf('<a href="%s">back to REST Admin example</a>', BASE_URL . 'rest_admin.php');