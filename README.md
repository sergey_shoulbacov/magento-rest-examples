# README #

### Example app setup
In *config.php* update BASE_URL, MAGENTO_BASE_URL constants,
set Config::$consumer_key and Config::$consumer_secret to your Magento REST OAuth Consumer (see below) Key and Secret

### Magento setup

## .htaccess
* Rewrite rule for API2 is required:
```
#!
RewriteRule ^api/rest api.php?type=rest [QSA,L]
```


## Creating Consumer
* In **System -> Web Services -> REST - OAuth Consumers** add new Consumer (leave Callback / Rejected Callback URL's blank).

## Customer setup
* In **System -> Web Services -> REST - Roles** select Customer role and allow Customer to retrieve products
* In **System -> Web Services -> REST - Attributes** select Customer and allow Customer to retrieve product Name attribute.

## Admin setup
* In **System -> Web Services -> REST - Roles** create new Admin Role and allow to retrieve Products
* In **System -> Web Services -> REST - Attributes** select created Role and allow to retrieve product Name attribute.
* In **System -> Permissions -> Users** select your Admin account and assign new REST Role.