<?php
define('BASE_URL', 'http://test.local/rest/');
define('MAGENTO_BASE_URL', 'http://ahava.me/');

class Config {
    public static $callback_url;
    public static $admin_callback_url;
    public static $temporary_credentials_request_url;
    public static $admin_temporary_credentials_request_url;
    public static $authorization_url;
    public static $admin_authorization_url;
    public static $access_token_request_url;
    public static $api_url;
    public static $consumer_key = 'byip7h6z33ccdh4z87x49kp36pnqi6vc' ;
    public static $consumer_secret = 'c4x4h7k6i2ns4a7m53j7spjl2rcz5io5';
}

Config::$api_url = MAGENTO_BASE_URL . 'api/rest';
Config::$access_token_request_url = MAGENTO_BASE_URL . 'oauth/token';

// Customer URLs
Config::$callback_url = BASE_URL . 'rest.php';
Config::$temporary_credentials_request_url =
    MAGENTO_BASE_URL . 'oauth/initiate?oauth_callback=' . urlencode(Config::$callback_url);
Config::$authorization_url = MAGENTO_BASE_URL . 'oauth/authorize';

// Admin URLs
Config::$admin_callback_url = BASE_URL . 'rest_admin.php';
Config::$admin_temporary_credentials_request_url =
    MAGENTO_BASE_URL . 'oauth/initiate?oauth_callback=' . urlencode(Config::$admin_callback_url);
Config::$admin_authorization_url = MAGENTO_BASE_URL . 'admin/oauth_authorize';
