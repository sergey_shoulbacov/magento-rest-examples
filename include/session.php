<?php
function session_has($name) {
    return isset($_SESSION[$name]);
}

function session_get($name) {
    return session_has($name) ? $_SESSION[$name] : null;
}

function session_set($name, $value) {
    $_SESSION[$name] = $value;
}

function session_clear($name) {
    unset($_SESSION[$name]);
}