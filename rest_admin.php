<?php
require_once 'include/init.php';
require_once 'include/config.php';
require_once 'include/session.php';

define('STATE_DEFAULT', 0);
define('STATE_REQUEST_TOKEN', 1); // Request Token acquired
define('STATE_ACCESS_TOKEN', 2);  // Access Token acquired, ready to use REST API

session_start();

$auth_type = (session_get('state') == STATE_ACCESS_TOKEN)
    ? OAUTH_AUTH_TYPE_AUTHORIZATION
    : OAUTH_AUTH_TYPE_URI;
// create OAuth client (requires PHP OAuth extension)
$oauth_client = new OAuth(
    Config::$consumer_key,
    Config::$consumer_secret,
    OAUTH_SIG_METHOD_HMACSHA1,
    $auth_type);
$oauth_client->enableDebug();

// if Request Token was acquired, but no OAuth Verifier (oauth_token) received
// then reset state and start over
if (!isset($_GET['oauth_token'])
    && session_has('state') && session_get('state') == STATE_REQUEST_TOKEN)
{
    session_set('state', STATE_DEFAULT);
}

try {
    if (!isset($_GET['oauth_token']) && !session_get('state')) {
        // get Request Token
        $request_token = $oauth_client->getRequestToken(
            Config::$admin_temporary_credentials_request_url);
        session_set('secret', $request_token['oauth_token_secret']);
        // update state
        session_set('state', STATE_REQUEST_TOKEN);

        // redirect to Magento Admin authorization URL with Request Token
        $header = 'Location: ' . Config::$admin_authorization_url
            . '?oauth_token=' . $request_token['oauth_token'];
        header($header);

    } elseif (session_get('state') == STATE_REQUEST_TOKEN) {
        // get Access Token
        $oauth_client->setToken($_GET['oauth_token'], session_get('secret'));
        $access_token = $oauth_client->getAccessToken(Config::$access_token_request_url);
        session_set('state', STATE_ACCESS_TOKEN);
        session_set('token', $access_token['oauth_token']);
        session_set('secret', $access_token['oauth_token_secret']);

        // reload
        $header = 'Location: ' . Config::$admin_callback_url;
        header($header);

    } else {
        // fetch data using Access Token
        $oauth_client->setToken(session_get('token'), session_get('secret'));
        $resource_url = Config::$api_url . '/products';
        $oauth_client->fetch(
            $resource_url,
            array(),
            OAUTH_HTTP_METHOD_GET,
            array('Accept' => 'application/json'));
        // var_dump($oauth_client->getLastResponse());
        $product_list = json_decode($oauth_client->getLastResponse());
        var_dump($product_list);
    }

} catch (Exception $e) {
    var_dump($e);
}

printf('<a href="%s">reset session</a>', BASE_URL . 'reset.php');
